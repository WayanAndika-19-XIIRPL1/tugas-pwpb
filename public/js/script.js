$(function(){

    $('.tambahDataModal').on('click',function(){

        $('#judulModal').html('Tambah Data Siswa');
        $('.modal-footer button[type=submit]').html('Tambah Data');
    });

    $('.tampilModalUbah').on('click', function(){
        
        $('#judulModal').html('Ubah Data Siswa');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.modal-body form').attr('action','http://localhost/phpmvc/public/siswa/ubah')
        const id=$(this).data('id');
        $.ajax({
            url : 'http://localhost/phpmvc/public/siswa/getubah',
            data: {id:id},
            method : 'post',
            dataType:'json',
            success:function(data){
                // console.log(data);
                $('#nama').val(data.nama);
                $('#user').val(data.username);
                $('#pass').val(data.password);
                $('#nis').val(data.nis);
                $('#kelas').val(data.kelas);
                $('#jurusan').val(data.jurusan);
                $('#jenisKelamin').val(data.jenis_kelamin);
                $('#id').val(data.id);
            }
        });
    });

});